def load_annotated_data(filename):
    result = []
    with open(filename, 'r') as data:
        # skipping the first two lines
        next(data)
        next(data)
        sent = []
        doc = []
        for line in (l.rstrip('\n') for l in data):
            if line == '-DOCSTART- -X- -X- O':
                result.append(doc)
                doc = []
            elif line == '':
                doc.append(sent)
                sent = []
            else:
                word, _, _, entity_annotation = line.split(' ')
                sent.append((word, entity_annotation))
        return result
