#!/usr/bin/env bash
mkdir -p data
cd data
wget https://s3-us-west-1.amazonaws.com/fasttext-vectors/cc.en.300.bin.gz
gunzip cc.en.300.bin.gz
