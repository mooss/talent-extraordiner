# Perspectives d'améliorations {.unnumbered}

Les modèles proposés dans ce travail se rapprochent fortement de l'état de l'art en termes de performances. Plusieurs perspectives sont envisageables afin de perfectionner ces approches.

Une première suggestion est la création d'un ensemble de modèles. Cette approche consiste à entraîner plusieurs modèles parallèlement avec quelques fluctuations dans les paramètres, puis de définir un vote à majorité sur les labels de sortie. Cette méthode donne généralement des résultats légèrement supérieurs aux modèles uniques.

Une deuxième piste serait d'ajouter une couche d'*embeddings* de caractères parallèle à celle dédiée aux tokens, puis de concaténer les vecteurs de caractères et de tokens afin de fournir des *embeddings* enrichis. Néanmoins, le modèle fastText apporte déjà une information morphologique riche à la fois dans les modèles à glissement de fenêtre et de séquence.
Comparer ces deux approches serait utile afin de déterminer laquelle est la plus appropriée à la tâche de reconnaissance d'entités nommées.
Il est également possible qu'elles soient complémentaires.

Une troisième perspective, dont l'intérêt se porte particulièrement au modèle d'étiquetage de séquences, serait l'ajout d'un mécanisme d'attention afin de pouvoir prédire une étiquette à chaque élément en prenant en compte toute la séquence.
Cela permettrait de mieux capturer l'information structurelle de la séquence d'entrée afin de mieux la modéliser dans la séquence de sortie.
Il s'agit donc de remplir le même rôle que le CRF mais en utilisant un réseau de neurones de bout à bout.
@VNER explorent cette approche, pour de la reconnaissance d'entités nommées vietnamiennes.

Finalement, une fonction de perte modifiée pour la tâche en cours serait également envisageable.
En effet, la fonction de perte d'entropie croisée pourrait être adaptée de manière à fournir une perte plus élevée lorsque des entités nommées sont prédites à tord ou lorsque des tokens qui ne sont pas des entités nommées sont prédits comme des entités nommées.
Une telle fonction de perte permettrait de refléter plus fidèlement le f1-score utilisé.

# Sources {.unnumbered}
