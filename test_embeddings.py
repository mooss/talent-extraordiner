from gensim.models import KeyedVectors
import pickle
import numpy as np
import torch
import torch.nn as nn

np.random.seed(seed=0)

# model = KeyedVectors.load_word2vec_format('../data/wiki-news-300d-1M.vec', limit=100000)
#
# pickle.dump(model, open('../data/wiki-news-300d-1M.p', 'wb'))

train_tokens = pickle.load(open('data/train_tokens.p', 'rb'))
train_labels = pickle.load(open('data/train_labels.p', 'rb'))

# words = []
# for doc in train_tokens:
#     for p in doc:
#         for w in p:
#              if w: words.append(w[0])
# words = set(words)

# model = pickle.load(open('../data/wiki-news-300d-1M.p', 'rb'))

# vec_dict = {w:model[w] if w in model else np.random.normal(scale=0.6, size=(300, )) for w in words}

# pickle.dump(vec_dict, open('data/vec_dict.p', 'wb'))

vec_dict = pickle.load(open('../data/vec_dict.p', 'rb'))
matrix_len = len(vec_dict)
weights_matrix = torch.Tensor(np.zeros((matrix_len, 300)))

"""
A FAIRE : Créer un dictionnaire de la forme {word:id}
"""

def create_emb_layer(weights_matrix, non_trainable=False):
    num_embeddings, embedding_dim = weights_matrix.size()
    emb_layer = nn.Embedding(num_embeddings, embedding_dim)
    emb_layer.load_state_dict({'weight': weights_matrix})
    if non_trainable:
        emb_layer.weight.requires_grad = False

    return emb_layer, num_embeddings, embedding_dim

class ToyNN(nn.Module):
    def __init__(self, weights_matrix, hidden_size, num_layers):
        super(self).__init__()
        self.embedding, num_embeddings, embedding_dim = create_emb_layer(weights_matrix, True)
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.gru = nn.GRU(embedding_dim, hidden_size, num_layers, batch_first=True)

    def forward(self, inp, hidden):
        return self.gru(self.embedding(inp), hidden)

    def init_hidden(self, batch_size):
        return Variable(torch.zeros(self.num_layers, batch_size, self.hidden_size))
