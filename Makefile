RAPPORT=AAL_rapport_alternants_nantes.pdf
TARGETS=$(RAPPORT)
PARTS=head.md mica.md felix.md tail.md

rapport: $(RAPPORT)

$(RAPPORT): $(PARTS)
	pandoc 	--standalone\
		--to latex $^\
		--output $@\
		--include-in-header preamble.tex\
		--highlight-style tango\
		--variable documentclass:report\
		--variable geometry:margin=2.5cm\
		--variable papersize:a4paper\
		--table-of-content\
		--filter pandoc-fignos\
		--filter pandoc-citeproc\
		--bibliography sources.bib


felix.md: rnn_notebook.org
	pandoc  --to markdown $^ --output $@ --atx-headers
	sed -ri $@\
		-e 's/``` \{[^.]*\.([^ ]+) .*\}/``` {.\1}/'\
		-e 's/\{\.ipython\}/{.python}/'\
		-e 's/^(#+) /#\1 /'\
		-e '1 i\# Classifieur de séquence (Félix)'
