import pickle

def preprocess(dataset):
    lines = [[[line for line in paragraph.split('\n') if line] for paragraph in doc.split('\n\n') if paragraph] for doc in dataset.split('-DOCSTART- -X- -X- O') if doc]
    tokens = [[[[line.split(' ')[0]][0] for line in paragraph] for paragraph in doc] for doc in lines]
    labels = [[[[line.split(' ')[-1]][0] for line in paragraph] for paragraph in doc] for doc in lines]
    return tokens, labels

def flatten(l):
    return [item for sublist in l for item in sublist]

train = open('../data/eng.train', 'r').read()
testa = open('../data/eng.testa', 'r').read()
testb = open('../data/eng.testb', 'r').read()

# for doc in text.split('-DOCSTART- -X- -X- O'):
#     for paragraph in doc.split('\n\n'):
#         for line in paragraph.split('\n'):
#             print(line)


train_tokens, train_labels = preprocess(train)
testa_tokens, testa_labels = preprocess(testa)
testb_tokens, testb_labels = preprocess(testb)

flattened_tokens = flatten(train_tokens)
flattened_labels = flatten(train_labels)

"""
Visually check if tokens and labels match
"""

train_tokens[-1]
train_labels[-1]

flattened_tokens[-1]
flattened_labels[-1]

pickle.dump(train_tokens, open('data/train_tokens.p', 'wb'))
pickle.dump(train_labels, open('data/train_labels.p', 'wb'))

pickle.dump(flattened_tokens, open('data/flat_train_tokens.p', 'wb'))
pickle.dump(flattened_labels, open('data/flattrain_labels.p', 'wb'))

testa_tokens[-1]
testa_labels[-1]

testb_tokens[-1]
testb_labels[-1]
