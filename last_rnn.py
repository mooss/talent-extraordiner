# force windows multiprocessing behavior on mac
import multiprocessing as mp
mp.set_start_method('spawn')

import time
import math
import torch
import torch.nn as nn
import pickle
from torch.autograd import Variable
from torch.utils.data import DataLoader

# from name_dataset import NameDataset
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

train_tokens = pickle.load(open('data/flat_train_tokens.p', 'rb'))
train_labels = pickle.load(open('data/flat_train_labels.p', 'rb'))
vec_dict = pickle.load(open('data/vec_dict.p', 'rb'))
all_labels =  ['I-ORG', 'O', 'I-MISC', 'I-PER', 'I-LOC', 'B-LOC', 'B-MISC', 'B-ORG']

token2id = {token: idx+1 for idx, token in enumerate(vec_dict.keys())}
label2id = {label: idx for idx, label in enumerate(all_labels)}

def create_variable(tensor):
    #Do cuda() before wrapping with variable
    if torch.cuda.is_available():
        return Variable(tensor.cuda())
    else:
        return Variable(tensor)

def sentence2vec(list_of_tokens):
    return [token2id[token] for token in list_of_tokens]

def make_batch(sentence, labels, window_size):
    sequence_batch = []
    label_batch = []
    for j, _ in enumerate(sentence):
        half_window = int(window_size/2)
        before, after = (j-half_window,j+half_window+1)
        before = before if before > 0 else 0
        padding_before, padding_after = 0, 0

        if j < half_window:
            padding_before = half_window - j
        if len(sentence) - j - 1 < half_window:
            padding_after = half_window - (len(sentence) - j - 1)

        sequence = sentence[before:after]
        sequence_vec = sentence2vec(sequence)
        # padding
        sequence_vec = torch.LongTensor([0]*padding_before + sequence_vec + [0]*padding_after)
        label = labels[j]
        label_idx = label2id[label]
        sequence_batch.append(sequence_vec)
        label_batch.append(label_idx)
    return create_variable(torch.stack(sequence_batch)), create_variable(torch.LongTensor(label_batch))


# Parameters and DataLoaders
HIDDEN_SIZE = 20
N_LAYERS = 2
N_EPOCHS = 1
WINDOW_SIZE = 5
TOTAL_TRAIN_LENGTH = len([token for sentence in train_tokens for token in sentence])
# Some utility functions
def time_since(since):
    s = time.time() - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

class RNNClassifier(nn.Module):
    # Our model

    def __init__(self, input_size, hidden_size, output_size, n_layers=1, bidirectional=True):
        super(RNNClassifier, self).__init__()
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.n_directions = int(bidirectional) + 1

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, n_layers,
                          bidirectional=bidirectional)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, input):
        # Note: we run this all at once (over the whole input sequence)
        # input shape: B x S (input size)
        # transpose to make S(sequence) x B (batch)
        input = input.t()

        batch_size = input.size(1)

        # Make a hidden
        hidden = self._init_hidden(batch_size)

        # Embedding S x B -> S x B x I (embedding size)
        embedded = self.embedding(input)

        # To compact weights again call flatten_parameters().
        self.gru.flatten_parameters()
        output, hidden = self.gru(embedded, hidden)

        # Use the last layer output as FC's input
        # No need to unpack, since we are going to use hidden
        fc_output = self.fc(hidden[-1])
        return fc_output

    def _init_hidden(self, batch_size):
        hidden = torch.zeros(self.n_layers * self.n_directions,
                             batch_size, self.hidden_size)
        return create_variable(hidden)


# Train cycle
def train():
    total_loss = 0
    tokens_done = 0
    for i, (sequence, labels) in enumerate(zip(train_tokens, train_labels), 1):
        input, target = make_batch(sequence, labels, window_size=WINDOW_SIZE)
        for t in target:
            try:
                assert t.item() in range(8)
            except:
                print(i, t)
        output = classifier(input)
        # print('---')
        # print(input, target, output)
        # print('---')
        # print(output.size(), target.size())
        loss = criterion(output, target)
        total_loss += loss.item()
        # except:
        #     print(sequence)
        #     print(labels)
        #     print(input)
        #     print(target)
        tokens_done += len(sequence)
        classifier.zero_grad()
        loss.backward()
        optimizer.step()
        # print("---")
        # print(input.size())
        # print(target.size())
        # print("---")

        if i % 100 == 0:
            print('[{}] Train Epoch: {} [{}/{} ({:.0f}%)]\t[{}/{} ({:.3f}%)]\tLoss: {:.2f}'.format(
                time_since(start), epoch, tokens_done, TOTAL_TRAIN_LENGTH,
                100. * tokens_done / TOTAL_TRAIN_LENGTH,
                tokens_done*N_EPOCHS, TOTAL_TRAIN_LENGTH*N_EPOCHS,
                100. * (tokens_done*epoch) / (TOTAL_TRAIN_LENGTH*N_EPOCHS),
                total_loss / i * len(sequence)))
    return total_loss


# Testing cycle
def test(name=None):
    # Predict for a given name
    if name:
        input, seq_lengths, target = make_variables([name], [])
        output = classifier(input, seq_lengths)
        pred = output.data.max(1, keepdim=True)[1]
        country_id = pred.cpu().numpy()[0][0]
        print(name, "is", train_dataset.get_country(country_id))
        return

    print("evaluating trained model ...")
    correct = 0
    train_data_size = len(test_loader.dataset)

    for names, countries in test_loader:
        input, seq_lengths, target = make_variables(names, countries)
        output = classifier(input, seq_lengths)
        pred = output.data.max(1, keepdim=True)[1]
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    print('\nTest set: Accuracy: {}/{} ({:.0f}%)\n'.format(
        correct, train_data_size, 100. * correct / train_data_size))


if __name__ == '__main__':
    N_WORDS = len(vec_dict) + 1
    N_LABELS = 8
    classifier = RNNClassifier(N_WORDS, HIDDEN_SIZE, N_LABELS, N_LAYERS)
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        # dim = 0 [33, xxx] -> [11, ...], [11, ...], [11, ...] on 3 GPUs
        classifier = nn.DataParallel(classifier)

    if torch.cuda.is_available():
        classifier.cuda()

    optimizer = torch.optim.Adam(classifier.parameters(), lr=0.001)
    criterion = nn.CrossEntropyLoss()

    start = time.time()
    print("Training for %d epochs..." % N_EPOCHS)
    for epoch in range(1, N_EPOCHS + 1):
        # Train cycle
        train()
    torch.save(classifier.state_dict(), "data/rnn_model-{}".format(start))

        # # Testing
        # test()
        #
        # # Testing several samples
        # test("Sung")
        # test("Jungwoo")
        # test("Soojin")
        # test("Nako")
clf = pickle.load(open('data/rnn_model.p', 'rb'))

# test_seq = torch.stack([torch.LongTensor([0, 0, sentence2vec(['EU'])[0], 0, 0]).cuda()])
# output = clf(test_seq)
# pred = output.data.max(1, keepdim=True)[1]
# all_labels[1]
