import time
import datetime
import math
import torch
import torch.nn as nn
import pickle
from torch.autograd import Variable

train_tokens = pickle.load(open('data/flat_train_tokens.p', 'rb'))
train_labels = pickle.load(open('data/flat_train_labels.p', 'rb'))
dev_tokens = pickle.load(open('data/flat_testa_tokens.p', 'rb'))
dev_labels = pickle.load(open('data/flat_testa_labels.p', 'rb'))

vec_dict = pickle.load(open('data/vec_dict.p', 'rb'))
all_labels =  ['I-ORG', 'O', 'I-MISC', 'I-PER', 'I-LOC', 'B-LOC', 'B-MISC', 'B-ORG']

vocab = list(vec_dict.keys()) + ["<unk>"]

token2id = {token: idx+1 for idx, token in enumerate(vocab)}
label2id = {label: idx for idx, label in enumerate(all_labels)}

def create_variable(tensor):
    #Do cuda() before wrapping with variable
    if torch.cuda.is_available():
        return Variable(tensor.cuda())
    else:
        return Variable(tensor)

def sentence2vec(list_of_tokens):
    return [token2id.get(token, token2id["<unk>"]) for token in list_of_tokens]

def make_batch(sentence, labels, window_size):
    sequence_batch = []
    label_batch = []
    for j, _ in enumerate(sentence):
        half_window = int(window_size/2)
        before, after = (j-half_window,j+half_window+1)
        before = before if before > 0 else 0
        padding_before, padding_after = 0, 0

        if j < half_window:
            padding_before = half_window - j
        if len(sentence) - j - 1 < half_window:
            padding_after = half_window - (len(sentence) - j - 1)

        sequence = sentence[before:after]
        sequence_vec = sentence2vec(sequence)
        # padding
        sequence_vec = torch.LongTensor([0]*padding_before + sequence_vec + [0]*padding_after)
        label = labels[j]
        label_idx = label2id[label]
        sequence_batch.append(sequence_vec)
        label_batch.append(label_idx)
    return create_variable(torch.stack(sequence_batch)), create_variable(torch.LongTensor(label_batch))


# Parameters and DataLoaders
HIDDEN_SIZE = 100
N_LAYERS = 2
N_EPOCHS = 100
WINDOW_SIZE = 5
TOTAL_TRAIN_LENGTH = len([token for sentence in train_tokens for token in sentence])
# Some utility functions
def time_since(since):
    s = time.time() - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

class RNNClassifier(nn.Module):
    # Our model

    def __init__(self, input_size, hidden_size, output_size, n_layers=1, bidirectional=True):
        super(RNNClassifier, self).__init__()
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.n_directions = int(bidirectional) + 1

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, n_layers,
                          bidirectional=bidirectional)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, input):
        # Note: we run this all at once (over the whole input sequence)
        # input shape: B x S (input size)
        # transpose to make S(sequence) x B (batch)
        input = input.t()

        batch_size = input.size(1)

        # Make a hidden
        hidden = self._init_hidden(batch_size)

        # Embedding S x B -> S x B x I (embedding size)
        embedded = self.embedding(input)

        # To compact weights again call flatten_parameters().
        self.gru.flatten_parameters()
        output, hidden = self.gru(embedded, hidden)

        # Use the last layer output as FC's input
        # No need to unpack, since we are going to use hidden
        fc_output = self.fc(hidden[-1])
        return fc_output

    def _init_hidden(self, batch_size):
        hidden = torch.zeros(self.n_layers * self.n_directions,
                             batch_size, self.hidden_size)
        return create_variable(hidden)

# Train cycle
def train():
    total_loss = 0
    tokens_done = 0
    for i, (sequence, labels) in enumerate(zip(train_tokens, train_labels), 1):
        input, target = make_batch(sequence, labels, window_size=WINDOW_SIZE)
        for t in target:
            try:
                assert t.item() in range(8)
            except:
                print(i, t)
        output = classifier(input)
        loss = criterion(output, target)
        total_loss += loss.item()
        tokens_done += len(sequence)
        classifier.zero_grad()
        loss.backward()
        optimizer.step()

        if i % 100 == 0:
            t = time_since(start)
            total_done = (tokens_done*epoch) / (TOTAL_TRAIN_LENGTH*N_EPOCHS)
            current_time = time.time()
            elapsed_time = current_time - start
            remaining_time = time_left = elapsed_time / total_done - elapsed_time

            print('[{}] Train Epoch: {} [{}/{} ({:.0f}%)]\t[{}/{} ({:.3f}%)]\tLoss: {:.2f}\tETA: {}'.format(
                t, epoch, tokens_done, TOTAL_TRAIN_LENGTH,
                100. * tokens_done / TOTAL_TRAIN_LENGTH,
                tokens_done*N_EPOCHS, TOTAL_TRAIN_LENGTH*N_EPOCHS,
                100. * total_done,
                total_loss / i * len(sequence),
                datetime.timedelta(seconds=remaining_time)
                ))
    return total_loss/len(train_tokens)

def test_dev():
    total_loss = 0
    for i, (sequence, labels) in enumerate(zip(dev_tokens, dev_labels), 1):
        input, target = make_batch(sequence, labels, window_size=WINDOW_SIZE)
        for t in target:
            try:
                assert t.item() in range(8)
            except:
                print(i, t)
        output = classifier(input)
        loss = criterion(output, target)
        total_loss += loss.item()
    return total_loss/len(dev_tokens)

if __name__ == '__main__':
    N_WORDS = len(vec_dict) + 2
    N_LABELS = 8
    classifier = RNNClassifier(N_WORDS, HIDDEN_SIZE, N_LABELS, N_LAYERS)
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        # dim = 0 [33, xxx] -> [11, ...], [11, ...], [11, ...] on 3 GPUs
        classifier = nn.DataParallel(classifier)

    if torch.cuda.is_available():
        classifier.cuda()

    optimizer = torch.optim.Adam(classifier.parameters(), lr=0.001)
    criterion = nn.CrossEntropyLoss()

    start = time.time()
    print("Training for %d epochs..." % N_EPOCHS)
    dev_losses = []
    for epoch in range(1, N_EPOCHS + 1):
        # Train cycle
        train_loss = train()
        dev_loss = test_dev()
        print("LOSS ON TRAIN: {}".format(train_loss))
        print("LOSS ON DEV: {}".format(dev_loss))
        dev_losses.append(dev_loss)
        if sorted(dev_losses).index(dev_loss) > 10:
            torch.save(classifier.state_dict(), "data/rnn_model-{}".format(start))
            break

    torch.save(classifier.state_dict(), "data/rnn_model-{}".format(start))
