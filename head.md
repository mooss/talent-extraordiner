---
title: Rapport d'apprentissage automatique en langue
author: Félix Jamet et Mica Ménard
---

# Introduction {.unnumbered}

L'objectif de ce travail est d'élaborer un système de reconnaissance d'entités nommées avec un réseau de neurones récurrent. Cette modalité d'apprentissage semble particulièrement appropriée à cette tâche, puisque ce type de réseau est généralement utilisé dans le contexte de la modélisation de séquences.

L'avantage des réseaux de neurones récurrents (que nous désignerons par l'abréviation "RNN") est leur capacité à modéliser l'historique d'une séquence à un instant $t$. Par ailleurs, la compression de l'historique générée par la cellule récurrente permet de s'affranchir des propriétés markoviennes d'une séquence $n$-grammes, laquelle ne pouvant prendre en compte un historique de taille 4 ou 5 au maximum.

## Tâche

La tâche de détection d'entitées nommées a de nombreuses applications et demeure l'une des plus populaires dans le domaine du traitement automatique du langage.

Une entité nommée est une instance référentielle du monde réel généralement référée par un nom propre. Ainsi, parmi les entités nommées, on compte généralement les noms de personnes, des organisations, des marques, etc.

Dans ce travail, nous nous intéresserons au jeu de données fourni par CONLL 2003, une campagne d'évaluation dont le but est la détection d'entités nommées. Ces données sont au format B-I-O, comme suit :

```
EU NNP I-NP I-ORG
rejects VBZ I-VP O
German JJ I-NP I-MISC
call NN I-NP O
to TO I-VP O
boycott VB I-VP O
British JJ I-NP I-MISC
lamb NN I-NP O
. . O O
```

La première colonne fait référence à la chaîne de caractères et les deuxième et troisième à la catégorie grammaticale. La quatrième spécifie si (1) la chaîne est une entité nommée et (2) si elle l'est, la nature de cette entité. Il existe quatre types d'entité :

* ORG : organisation
* MISC : divers
* PER : personne
* LOC : entité géographique

Les préfixes "I-" et "B-" signifient respectivement "à l'intérieur de" (*inside*) et "début de" (*beginning*). Le label "O" signifie qu'il ne s'agit pas d'une entité nommée (*outside*).

## Mesure d'évaluation

Pour la reconnaissance d'entité nommées, un f1-score modifié est typiquement utilisé.
Le besoin d'un score différent émerge du fait que dans un texte, seule une faible partie des mots sont des entités nommées.
Ainsi, en utilisant un f1-score standard, il est aisé d'obtenir un f1-score de plus de 80% en prédisant uniquement le label "O".
Ce qui donne un f1-score peu intuitif et rend les comparaisons entre deux systèmes plus difficiles.

Plusieurs variations existent, certaines utilisent les *chunks* comme unité de base tandis que d'autres utilisent les *tokens*.
@CoNLL_task présentent la mesure utilisée de la manière suivante :

> Precision is the percentage of named entities found by the learning
> system that are correct. Recall is the percentage of named entities
> present in the corpus that are found by the system. A named entity is
> correct only if it is an exact match of the corresponding entity in
> the data file.

Nous avons interprété cette définition comme signifiant que le label "O" n'est jamais compté lorsqu'on le prédit correctement mais il est compté lorsque le système prédit un autre label à sa place.
Nous avons considéré que l'unité de base de cette définition est le *token* et non pas le *chunk*.
Notre interprétation s'est traduite en la fonction suivante :
``` {.python}
def ner_f1_score(trues, preds, outside):
    correct_pred_entity = incorrect_pred_entity = 0
    found_true_entity = notfound_true_entity = 0
    for true, pred in zip(trues, preds):
        if pred != outside:
            if pred == true:
                correct_pred_entity += 1
            else:
                incorrect_pred_entity += 1
        if true != outside:
            if true == pred:
                found_true_entity += 1
            else:
                notfound_true_entity += 1

    total_pred_entity = correct_pred_entity + incorrect_pred_entity
    precision = correct_pred_entity / (total_pred_entity) if total_pred_entity != 0 else 0

    total_true_entity = found_true_entity + notfound_true_entity
    recall = found_true_entity / (total_true_entity) if total_true_entity != 0 else 0

    f1 = (2 * precision * recall) / (precision + recall) if precision + recall != 0 else 0
    return f1
```


Pour montrer l\'impact de cette fonction de mesure, on peut la comparer
au *f1-score* standard, tel que fourni par sklearn. On suppose que
l\'ensemble de test est chargé dans `test_set` sous forme d\'une liste
de tuples $\langle \text{token}, \text{label} \rangle$.

On peut observer le déséquilibre des classes :

``` {.python}
from collections import Counter
labels = [label for _, label in test_set]
print(Counter(labels))
```

::: {.RESULTS .drawer}
Counter({\'O\': 38089, \'I-PER\': 2762, \'I-ORG\': 2489, \'I-LOC\': 1907, \'I-MISC\': 898, \'B-MISC\': 9, \'B-LOC\': 6, \'B-ORG\': 5})
:::

On compare les deux fonctions de f1-score avec le code suivant :

``` {.python}
from sklearn.metrics import f1_score
outside_pred = ['O'] * len(labels)
print('f1 score standard =', f1_score(labels, outside_pred, average='micro'))
print('f1 score adapté à la tâche =', ner_f1_score(labels, outside_pred, 'O'))
```

::: {.RESULTS .drawer}
f1 score standard = 0.8250622766164843

f1 score adapté à la tâche = 0
:::

On constate bien que prédire *outside* en permanence avec cette mesure n\'apporte jamais aucun point étant donné qu\'elle ne récompense que les entités correctement prédites.

## État de l'art

La reconnaissance d'entité nommée est une tâche bien connue dans le domaine du traitement du langage naturel.
L'état de l'art de CoNLL-2003 a été répétitivement amélioré ces dernières années via des architectures neuronales.
@lample_et_al utilisent un LSTM bidirectionnel, des *word embeddings* pré-entrainés, des *embeddings* de caractères et un CRF pour atteindre un F1-score de 90.94.
Simultanément, @yang_et_al ont étudié l'impact du *joint learning* sur diverses tâches d'étiquetage de séquences, dont la reconnaissance d'entité nommée.

@road_to_dl fournit une excellente explication du domaine de la reconnaissance d'entités nommées, des améliorations apportées par les modèles neuronaux et du rôle des composants utilisés dans un système de reconnaissance d'entité nommées état de l'art.

## Vue d'ensemble

Notre travail présentera deux types de modèles récurrents proposant chacun une solution au problème de la reconnaissance d'entités nommées.
Le premier modèle présente une architecture *sequence to one* par fenêtre glissante pour classifier une séquence donnée.
Le deuxième modèle suit directement une architecture d'étiquetage de séquence.

L'élaboration des différents modèles a été réalisée en deux étapes.
Le premier modèle à fenêtre glissante a tout d'abord été conçu. Par la suite, en vue des faiblesses de ce dernier, le deuxième modèle de séquence a été mis en oeuvre.

Lors d'une deuxième étape, après introspection quand à leurs points faibles, les modèles initiaux ont été soumis à quelques améliorations.


### Embeddings

Le choix a été fait d'utiliser des *embeddings* pré-entrainés pour pouvoir exploiter facilement des caractéristiques extraites sur de plus grandes bases de données que celle à notre disposition.
L'approche $n$-grammes de caractères proposée par fastText nous a semblé très pertinente car elle permet de générer des *embeddings* morphologiquement cohérents pour des mots jamais observés.
En effet, les entités nommées sont hautement susceptibles de ne pas être présentes dans la liste des *embeddings*.
C'est pourquoi fastText a été choisi parmi autres approches telles que GloVe ou Word2vec pour les modèles finaux, bien que des expériences aient été également réalisées initialement avec GloVe.

Nous avons mis à disposition le script `install_fasttext.sh` sur le git.
Ce script devrait suffir à télécharger et installer fastText sur une
machine linux avec un compilateur `C++11`. Il est également visible
ci-dessous.

``` {.bash}
tmpfolder=$(mktemp -d)
cd $tmpfolder
git clone https://github.com/facebookresearch/fastText.git
cd fastText
if [ $(env | grep VIRTUAL_ENV) ]
then # inside virtualenv
    pip install numpy scipy pybind11 .
else # outside virtualenv, needs to use --user
    pip install --user numpy scipy pybind11 .
fi

cd -
rm -fr $tmpfolder
```

### Cellules récurrentes à porte

L'article de @gru_vs_lstm est une étude préliminaire entre les cellules RNN "vanilla", les cellules GRU et les cellules LSTM.
Cette étude conclue que les performances entre GRU et LSTM semblent comparables.
Une différence notable entre ces deux cellules est que les GRU semblent converger plus rapidement.

On peut toutefois remarquer que les deux articles sur lesquels nous nous sommes basé utilisent eux des LSTM.
Les LSTM ont donc certainement des avantages que nous ignorons.
Cependant, ce projet étant surtout un projet de découverte des réseaux de neurones, il nous a semblé préférable d'utiliser le type de cellule nous permettant d'itérer plus rapidement.
