# Classifieur de token à fenêtre glissante (Mica)

Ce modèle tente de résoudre la tâche de reconnaissance d'entités nommées avec une architecture à fenêtre glissante. Après avoir spécifié les caractéristiques du modèle de base, nous nous focaliserons sur les modifications apportées.

## Architecture et paramètres

Le modèle à fenêtre glissante est une modèle *sequence to one*, ce qui signifie qu'un label unique est généré à partir d'une séquence de tokens. L'idée est d'exécuter le modèle pour chaque token de la phrase individuellement sur une fenêtre de taille fixe afin de déterminer le label de chacun des tokens.

Ainsi, le modèle devine le label du mot central de la fenêtre. Afin de résoudre d'éventuels problèmes au début et à la fin des séquences, un *padding* (`<pad>`) est introduit.

De manière arbitraire, nous avons choisi une fenêtre de taille 5.

![Glissement par fenêtre sur une séquence de tokens](img/window_sliding.svg){#fig:sliding_window width=400px}

### Modèle de base

La base du modèle utilise des vecteurs GloVe dans sa couche d'embeddings. Ceux-ci sont entraînés sur le *Corpus Common Crawl*, lequel comporte 6 milliards de tokens. De la manière du token `<pad>`, un vecteur `<unk>` représentant les mots inconnus a été ajouté à cette couche d'embeddings.

Lors de l'entraînement, cette couche est soumise à la rétropropagation. Suite à une mauvaise interprétation des résultats, la couche softmax a été omise avant d'être réintégrée dans la version finale. Dû à un surapprentissage du modèle mis en évidence sur la figure @fig:model1-loss , le meilleur modèle est celui de la troisième époque.

![Perte du modèle de base sur plusieurs époques](img/losses_w_legend.svg){#fig:model1-loss width=500px}

Une autre faiblesse de ce modèle est la lenteur de l'apprentissage. En effet, celui-ci nécessite cinq heures d'entraînement pour trois époques. La raison pour cela est la rétropropagation de la couche d'embeddings, qui est une matrice comportant 400 002 lignes, c'est-à-dire le nombre de tokens uniques dans le modèle GloVe (avec le *padding* et le token inconnu).

Enfin, la F-mesure sur le corpus de test de ce modèle est de 0.66, ce qui reste loin de l'état de l'art.

### Améliorations

Afin de résoudre ces trois problèmes (soit le surapprentissage, la lenteur de l'entraînement et surtout la faible F-mesure), plusieurs méthodes ont été testées. Cependant, chacune des variables n'a pas pu être testée indépendamment par contrainte de temps.

Premièrement, pour résoudre le problème du surapprentissage, un *dropout* de 0.2 a été ajouté sur la couche dense.

Deuxièmement, afin d'accélérer le processus d'apprentissage, la couche d'embeddings a été fixée de manière à ne pas être soumise à la rétropropagation. En revanche, ceci a pour effet de diminuer la F-mesure (0.63 sur trois époques et corpus de test).

Troisièmement, afin de pallier une fois encore à la lenteur de l'entraînement et à la faible performance, un autre modèle d'embeddings plus adapté a été envisagé. fastText est un modèle produisant des embeddings non seulement pour les tokens déjà rencontrés lors son entraînement, mais également pour les $n$-grammes de ses tokens. En somme, ce modèle prend en compte les aspects morphologiques de chaque token et est ainsi capable de générer des embeddings pour les mots inconnus, ce qui dispense de l'utilisation du token superficiel `<unk>`.

Enfin, une couche softmax a été ajoutée par-dessus la couche dense.

### Architecture finale

L'architecture finale du modèle est représentée sur la figure @fig:model1-archi.

![Architecture du modèle 1](img/model1.svg){#fig:model1-archi width=500px}

La couche d'embedding a été remplacée directement par le modèle fastText, lequel n'est pas soumis à la rétropropagation. Deux cellules GRU représentées sur le schéma sont en réalité quatre couches GRU (deux pour chaque direction). Ceci génère une couche cachée de dimensions 4 x 300. Il est important de noter que chaque cellule GRU prend une entrée de dimension 300 **mais émet une sortie de taille 100**. La couche dense est de dimension 100 et est soumise à un *dropout* de 0.2; laquelle transmet l'information à la couche softmax afin de prendre la décision quant au label de sortie.

Le token `<pad>` n'est pas representé par fastText mais par un vecteur nul de dimension 300.

### Paramètres d'entraînement

L'entraînement du modèle optimal s'effectue sur 13 époques avec un taux d'apprentissage de 0.001 et un *dropout* de 0.2 sur la couche dense. L'optimiseur utilisé est Adam et la fonction de perte est l'entropie croisée.

## Prétraitements

Pour ce type de données, les prétraitements sont minimes. La méthode `preprocess` divise le jeu de données en documents, puis en paragraphes (séparés par des doubles retours à la ligne), puis des lignes, qui représentent le niveau des phrases dans les données.

Les tokens et labels sont ensuite extraits de la variable `lines`.

```{.python}
def preprocess(dataset):
    lines = [[[line for line in paragraph.split('\n') if line]
              for paragraph in doc.split('\n\n') if paragraph]
              for doc in dataset.split('-DOCSTART- -X- -X- O') if doc]
    tokens = [[[[line.split(' ')[0]][0] for line in paragraph]
                for paragraph in doc] for doc in lines]
    labels = [[[[line.split(' ')[-1]][0] for line in paragraph]
              for paragraph in doc] for doc in lines]
    return tokens, labels

def flatten(l):
    return [item for sublist in l for item in sublist]

```

La méthode `flatten` sert à réduire une liste à $n$ dimensions à une liste à $n-1$ dimensions. En effet, les variables `tokens` et `labels` sont de dimensions 4, que l'on peut représenter comme ceci :

```
Document
-- Paragraphe
---- Phrase
------ Token (ou Label)
```

`flatten` réduit ainsi les documents en listes de phrases de tokens (ou de labels), comme par exemple :

```{.python}
[['SOCCER',
  '-',
  'JAPAN',
  'GET',
  'LUCKY',
  'WIN',
  ',',
  'CHINA',
  'IN',
  'SURPRISE',
  'DEFEAT',
  '.'],
 ['Nadim', 'Ladki'],
 ...
 ['Zahoor', 'Elahi', 'b', 'Cairns', '86', '(', 'corrects', 'from', '87', ')']]
```

Voici un exemple d'application de prétraitements sur le corpus de test :

```{.python}
testb = open('path_to_eng.testb', 'r').read()
testb_tokens, testb_labels = preprocess(testb)
flattened_tokens = flatten(train_tokens)
flattened_labels = flatten(train_labels)
```

## Implémentation du réseau

L'implémentation du modèle RNN suit la structure suivante :

```{.python}
class RNNClassifier(nn.Module):
    # Our model
    def __init__(self, hidden_in, hidden_out, output_size, n_layers=1, bidirectional=True):
        ...
    def embed(self, word):
        ...
    def forward(self, input):
        ...
    def _init_hidden(self, batch_size):
        ...
```

Voici l'implémentation de chacune de ces méthodes :

```{.python}
def __init__(self, hidden_in, hidden_out, output_size, n_layers=1, bidirectional=True):
    super(RNNClassifier, self).__init__()
    # The input and output sizes of the hidden layer can be different
    self.hidden_in = hidden_in
    self.hidden_out = hidden_out
    # number of GRU layers
    self.n_layers = n_layers
    self.n_directions = int(bidirectional) + 1
    self.drop_layer = nn.Dropout(p=0.2)
    self.gru = nn.GRU(hidden_in, hidden_out, n_layers,
                      bidirectional=bidirectional)
    self.fc = nn.Linear(hidden_out, output_size)
```

La méthode `embed` génère un vecteur numpy de 300 dimensions à partir d'une chaîne de caractères. Si toutefois, cette chaîne de caractère est `<pad>`, le vecteur retourné est nul.

```{.python}
def embed(self, word):
    if word != "<pad>":
        return fastText_model.get_word_vector(word)
    else:
        return np.zeros(self.hidden_in)
```

Le code ci-dessous sert à exécuter le processus *feed-forward* :

```{.python}
def forward(self, input):
    # Transforming input to tensor
    tensor =  [[self.embed(w) for w in sentence] for sentence in input]
    embeddings = torch.tensor(tensor, dtype=torch.float).permute(1, 0, 2).cuda()

    batch_size = embeddings.size(1)

    # Initialize hidden layer
    hidden = self._init_hidden(batch_size)

    # To compact weights again call flatten_parameters()
    self.gru.flatten_parameters()
    output, hidden = self.gru(embeddings, hidden)

    fc_output = self.drop_layer(self.fc(hidden[-1]))
    out = nn.functional.log_softmax(fc_output, dim=0)
    return fc_output

    def _init_hidden(self, batch_size):
        hidden = torch.zeros(self.n_layers * self.n_directions,
                             batch_size, self.hidden_out)
        return create_variable(hidden)
```

## Entraînement et évaluation du modèle

Avant de présenter les fonctions d'entraînement et d'évaluation, il convient d'expliquer comment les batches sont générés. Chaque batch correspond à une phrase du corpus (séquence de tokens terminant par un point). Voici la méthode concernée.

```{.python}
def make_batch(sentence, labels, window_size):
    sequence_batch = []
    label_batch = []
    for j, _ in enumerate(sentence):
        half_window = int(window_size/2)
        before, after = (j-half_window,j+half_window+1)
        before = before if before > 0 else 0
        padding_before, padding_after = 0, 0

        if j < half_window:
            padding_before = half_window - j
        if len(sentence) - j - 1 < half_window:
            padding_after = half_window - (len(sentence) - j - 1)

        sequence = ["<pad>"]*padding_before + sentence[before:after] + ["<pad>"]*padding_after
        label = labels[j]
        label_idx = label2id[label]
        sequence_batch.append(sequence)
        label_batch.append(label_idx)
    return sequence_batch, create_variable(torch.LongTensor(label_batch))
```

La méthode `create_variable` permet de créer un objet `Variable` en mode CUDA si un GPU est disponible, en mode CPU sinon.

Voici une version minimale de la méthode permettant d'entraîner le modèle.

```{.python}
def train():
    # making sure the classifier is in train mode
    classifier.train()
    total_loss = 0
    tokens_done = 0
    for i, (sequence, labels) in enumerate(zip(train_tokens, train_labels), 1):
        input, target = make_batch(sequence, labels, window_size=WINDOW_SIZE)
        output = classifier(input)
        loss = criterion(output, target)
        total_loss += loss.item()
        tokens_done += len(sequence)
        classifier.zero_grad()
        loss.backward()
        optimizer.step()

    return total_loss/len(train_tokens)
```

Le code suivant permet d'évaluer le modèle à la fin de chaque époque.

```{.python}
def test_dev():
    # making sure the classifier is in evaluation mode
    classifier.eval()
    total_loss = 0
    all_preds = []
    for i, (sequence, labels) in enumerate(zip(dev_tokens, dev_labels), 1):
        input, target = make_batch(sequence, labels, window_size=WINDOW_SIZE)
        output = classifier(input)
        loss = criterion(output, target)
        total_loss += loss.item()
        predictions = [int2label(item.item())
                      for item in output.data.max(1, keepdim=True)[1]]
        all_preds.append(predictions)
    all_preds = [item for sublist in all_preds for item in sublist]
    all_labels = [item for sublist in dev_labels for item in sublist]
    f1 = ner_f1_score(all_labels, all_preds, 'O')
    print("F1-scores: {}".format(f1s))
    return total_loss/len(dev_tokens), f1
```

La fonction `ner_f1_score` est présentée dans la partie concernant le classifieur de séquences.

Le script suivant permet d'évaluer le modèle final sur le jeu de test :

```{.python}
all_labels =  ['I-ORG', 'O', 'I-MISC', 'I-PER', 'I-LOC', 'B-LOC', 'B-MISC', 'B-ORG']
classifier = RNNClassifier(N_WORDS, HIDDEN_SIZE, N_LABELS, N_LAYERS).cuda()
classifier.load_state_dict(torch.load(model))
classifier.eval()
all_preds = []
for sentence, labels in zip(dev_tokens, dev_labels):
    token_tensor, label_tensor = make_batch(sentence, labels, window_size=WINDOW_SIZE)
    output = classifier(token_tensor)
    predictions = [all_labels[item.item()] for item in output.data.max(1, keepdim=True)[1]]
    all_preds.append(predictions)
# flattening all_preds and all_labels
all_preds = [item for sublist in all_preds for item in sublist]
all_labels = [item for sublist in dev_labels for item in sublist]
f1 = ner_f1_score(all_labels, all_preds, 'O')
print("F1-score: {}".format(f1))
```

Enfin, voici les paramètres fournis au modèle :

```{.python}
HIDDEN_IN = 300
HIDDEN_OUT = 100
N_LAYERS = 2
N_EPOCHS = 13
WINDOW_SIZE = 5
```

## Résulats et analyse

Pour récapituler, plusieurs stratégies ont été adoptées afin d'améliorer les performances du modèle de base. Dans un premier temps, le *dropout* ainsi que la réduction de la taille de sortie des cellules GRU ont pour but d'éviter sur le surapprentissage. Dans un deuxième temps, un objectif était également de fixer la couche d'*embeddings* pour accélérer l'entraînement. Enfin, le modèle d'*embeddings* fastText a substitué le modèle GloVe afin de mieux représenter les chaînes de caractères et de générer des vecteurs pour les mots inconnus.

Nous constatons sur la figure @fig:model1-fscore que la stratégie du *dropout* n'a que peu d'impact sur les résultats. A posteriori, la réduction de la taille de sortie des cellules GRU aurait plus d'impact, comme le montre les courbes orange et bleue. Le *dropout* apporterait cependant une amélioration marginale puisque la courbe bleue atteint son maximum aux alentours d'une F-mesure de 0.93.

![Résultats de la F-mesure sur plusieurs époques](img/final_f1s.svg){#fig:model1-fscore width=500px}

Le résultat final sur le corpus de test est une F-mesure de **0.89**, ce qui se rapproche de l'état de l'art.

Toutefois, nous attribuons le crédit de ces améliorations en performance au modèle fastText, puisqu'il s'agit de l'unique paramètre poussant drastiquement les résultats vers le haut. En effet, le modèle de base produisait une F-mesure de 0.66 sur la troisième époque tandis que le nouveau résulte en une F-mesure d'au moins 0.91 dès la deuxième époque. Les embeddings fastText font ainsi gagner environ 25 points de précision/rappel.

Il convient cependant d'affirmer que ce modèle est toujours relativement long à entraîner comparé au modèle de séquence de la partie subséquente, qui offre des résultats comparables avec un temps d'entraînement beaucoup plus faible. Nous en concluons que le glissement de fenêtre représente un coup non négligeable puisque celui-ci multiplie le nombre d'opérations pour une phrase donnée.

## Autres perspectives d'applications

Les réseaux de neurones récurrents offrent de nombreuses applications du fait de leur capacité à modéliser des séquences. Dans le domaine du traitement automatique du langage naturel, les RNN peuvent s'avérer particulièrement efficaces là où échouent les systèmes à base de règles.

Une application spécifique est l'étiquetage morphosyntaxique (*POS tagging*). La version de base de ce modèle a été testée dans le contexte de cette tâche sur le turc, qui est une langue à morphologie agglutinante, c'est-à-dire dont la génération de mots s'effectue par suffixation récursive. Le corpus utilisé est le corpus BOUN ^[Corpus BOUN : http://st2.zargan.com/duyuru/Zargan_Linguistic_Resources_for_Turkish.html, jeu de données : word_forms_stems_and_frequencies_full.zip]. L'entraînement à été utilisé sur un jeu de données correspondant à un tableau comportant 1,5 millions de lignes avec chacune la forme brute d'un mot, son lemme, sa dérivation morphologique et son nombre d'occurences dans le corpus.

Puisque ces données ne comportent que des tokens individuels, le modèle RNN a été adapté de telle sorte à ce que la séquence d'entrée soit une séquence de caractère. La couche d'*embeddings* (de caractères) de dimensions 30 a été initialisée aléatoirement et mise à jour lors de l'entraînement. La tâche précise est de deviner la catégorie grammaticale d'un mot (adjectif, adverbe, conjonction, déterminant, interjection, nom, clitique, pronom, quantifieur, marqueur de question ou verbe).

La F-mesure obtenue un échantillon de test de 20% du jeu de données totale est de 0.88.

Par ailleurs, on pourrait appliquer une modèle de séquence (comme celui présenté dans la partie suivante) sur les mêmes données afin d'entraîner un lemmatiseur. Ces procédés semblent hautement généralisables sur d'autres langues, comme l'anglais ou le français, qui ne sont pas morphologiquement aussi sophisitiquées.
