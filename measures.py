def ner_f1_score(trues, preds, outside):
    correct_pred_entity = incorrect_pred_entity = 0
    found_true_entity = notfound_true_entity = 0
    for true, pred in zip(trues, preds):
        if pred != outside:
            if pred == true:
                correct_pred_entity += 1
            else:
                incorrect_pred_entity += 1
        if true != outside:
            if true == pred:
                found_true_entity += 1
            else:
                notfound_true_entity += 1

    total_pred_entity = correct_pred_entity + incorrect_pred_entity
    precision = correct_pred_entity / (total_pred_entity) if total_pred_entity != 0 else 0

    total_true_entity = found_true_entity + notfound_true_entity
    recall = found_true_entity / (total_true_entity) if total_true_entity != 0 else 0

    f1 = (2 * precision * recall) / (precision + recall) if precision + recall != 0 else 0
    return f1
