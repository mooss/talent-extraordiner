#!/usr/bin/env bash
tmpfolder=$(mktemp -d)
cd $tmpfolder
git clone https://github.com/facebookresearch/fastText.git
cd fastText
if [ $(env | grep VIRTUAL_ENV) ]
then # inside virtualenv
    pip install numpy scipy pybind11 .
else # outside virtualenv, needs to use --user
    pip install --user numpy scipy pybind11 .
fi

cd -
rm -fr $tmpfolder
