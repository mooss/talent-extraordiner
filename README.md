# Articles réfs du sujet du projet

* https://arxiv.org/abs/1803.05449
* https://arxiv.org/abs/1804.07461
* http://people.csail.mit.edu/jrg/meetings/ibm-internship-summary-sls-talk.pdf
* https://medium.com/huggingface/universal-word-sentence-embeddings-ce48ddc8fc3a

# Instructions de connexion

## Sur un réseau non bloqué

L'identifiant du mans est de la forme s123456.
```sh
ssh identifiant@transit.univ-lemans.fr
ssh skinner
ssh gpue1
```

Ou en une ligne (nécessite cependant de taper deux fois le mdp) :
```sh
ssh -A -t -l identifiant_univ_lemans transit.univ-lemans.fr ssh -A -t skinner ssh -A gpue1
```

## Sur le réseau de la fac

Pour passer à travers le réseau de la fac, établir un tunnel ssh entre bastion et transit puis se connecter en l'utilisant :
```sh
ssh -f -N -L:port_local:transit.univ-lemans.fr:22 identifiant_univ_nantes@bastion.etu.univ-nantes.fr
ssh -A -t identifiant_lemans@localhost -p port_local ssh -A -t skinner ssh -A gpue1
```

Où
 - port_local est un port TCP libre supérieur à 1024 (basiquement un numéro quelquonque entre 1024 et 65535)
 - identifiant\_univ\_nantes est le numéro d'étudiant de nantes, de la forme EXXXXXXL

Possible en une ligne, en sachant qu'il n'est pas nécessaire de réétablir le tunnel ssh si il est déjà en place :
```sh
ssh -f -N -L:port_local:transit.univ-lemans.fr:22 identifiant_univ_nantes@bastion.etu.univ-nantes.fr && ssh -A -t identifiant_lemans@localhost -p port_local ssh -A -t skinner ssh -A gpue1
```

En utilisant cette commande, il faut donc entrer une fois le mot de passe univ nantes et deux fois le mot de passe univ lemans.

# Papiers
Il y a probablement tout ce qu'il faut dans les deux premiers papiers (d'un point de vue architecture du réseau).

Collobert et al. (2011) est souvent mentionné.

## Multi-Task Cross-Lingual Sequence Tagging from Scratch

Disponible dans le [repo](Multi-Task_Cross-Lingual_Sequence_Tagging_from_Scratch.pdf).

 - Couvre plusieurs tâches dont la NER.
 - Embeddings de mots et de caractères.
 - GRU et CRF.
 - Bien écrit, bonne description des GRU.
 - Réseau bidirectionnel (lit la phrase dans les deux sens pour faire ses représentations).
 - Certains trucs nous concernent pas (entrainement sur plusieurs langues et sur plusieurs tâches).
 - N'utilise pas de "hand-crafted features".

Parle d'un travail un peu similaire et paru en même temps mais focus sur le NER : le papier d'après.

## Neural Architectures for Named Entity Recognition

Disponible sur le [repo](Neural_Architectures_for_Named_Entity_Recognition.pdf).

 - Spécifique à la NER.
 - LSTM et CRF.
 - Là encore, réseau bidirectionnel.
 - Utilise *character-based representation model* **et** *distributionnal representation* (Mikolov).
 Utilisation de *dropout* pour encourager le modèles à prendre les deux représentations en compte.
 Le *dropout* permet au modèle de mieux généraliser.
 - Utilisation de IOBES au lieu de IOB mais ne donne pas d'amélioration significative pour eux.
 - *Character-embeddings* de dimension 25 pour *forward* comme pour *backward* donc 50 en tout.
 - SGD avec *learning rate* de 0.01 et *gradient clipping* de 5.0.
 - Une couche LSTM *backward* et une couche LSTM *forward* de dimension 100 chacune.


## Empirical Evaluation of Gated Recurrent Neural Networks on Sequence Modeling

Disponible sur [arxiv](https://arxiv.org/pdf/1412.3555v1.pdf).

Comparaison entre RNN "vanilla" (tanh gate), GRU et LSTM.

 - Étude préliminaire, ne s'avance donc pas trop.
 - Effectué sur de l'audio donc potentiellement différent du texte.
 - Performances comparables entre LSTMs et GRUs.
 - Les GRUs semblent converger un peu plus rapidement.
 Les réponses à [ce post sur stackexchange](https://datascience.stackexchange.com/questions/14581/when-to-use-gru-over-lstm) s'accordent là dessus et les GRUs y sont décrits comme plus simple.

## CoNLL task presentation

 - https://www.aclweb.org/anthology/W03-0419

# Points techniques importants

 - Les CRFs dans la dernière couche permettent de prendre tous les éléments de la séquence (tous les mots) et de leurs assigner leurs tags.
 C'est grâce à ça que les tags de type BIO sont assignés.

# Autres ressources

 - https://www.quora.com/What-is-the-current-state-of-the-art-in-Named-Entity-Recognition-NER
 Question quora très pertinente à notre projet.

# Variable length batches
 - https://stackoverflow.com/questions/49466894/how-to-correctly-give-inputs-to-embedding-lstm-and-linear-layers-in-pytorch
 Batches, padding, packing, LSTM and linear layers.

 - https://ryankresse.com/dealing-with-pad-tokens-in-sequence-models-loss-masking-and-pytorchs-packed-sequence/
 Padding, packing and masking.

 - https://discuss.pytorch.org/t/batch-processing-with-variable-length-sequences/3150/4
 Padding, packing and masking.

 - https://towardsdatascience.com/taming-lstms-variable-sized-mini-batches-and-why-pytorch-is-good-for-your-health-61d35642972e
 
## NER and the road to deep learning
lien : http://nlp.town/blog/ner-and-the-road-to-deep-learning/.

Superbe explication de l'évolution de la NER et du rôle de chaque composant dans les modèle DL modernes (character-based embeddings, bidirectionnal LSTMs, CRFs, words embeddings).
Contient également des valeurs d'hyperparamètres et des liens potentiellement utiles.
