import time
import datetime
import math
import torch
import torch.nn as nn
import pickle
from torch.autograd import Variable
import sys
from torchvision import models

train_tokens = pickle.load(open('data/flat_train_tokens.p', 'rb'))
train_labels = pickle.load(open('data/flat_train_labels.p', 'rb'))
vec_dict = pickle.load(open('data/vec_dict.p', 'rb'))
all_labels =  ['I-ORG', 'O', 'I-MISC', 'I-PER', 'I-LOC', 'B-LOC', 'B-MISC', 'B-ORG']

token2id = {token: idx+1 for idx, token in enumerate(vec_dict.keys())}
label2id = {label: idx for idx, label in enumerate(all_labels)}

def create_variable(tensor):
    #Do cuda() before wrapping with variable
    if torch.cuda.is_available():
        return Variable(tensor.cuda())
    else:
        return Variable(tensor)

def sentence2vec(list_of_tokens):
    return [token2id[token] for token in list_of_tokens]

def make_batch(sentence, labels, window_size):
    sequence_batch = []
    label_batch = []
    for j, _ in enumerate(sentence):
        half_window = int(window_size/2)
        before, after = (j-half_window,j+half_window+1)
        before = before if before > 0 else 0
        padding_before, padding_after = 0, 0

        if j < half_window:
            padding_before = half_window - j
        if len(sentence) - j - 1 < half_window:
            padding_after = half_window - (len(sentence) - j - 1)

        sequence = sentence[before:after]
        sequence_vec = sentence2vec(sequence)
        # padding
        sequence_vec = torch.LongTensor([0]*padding_before + sequence_vec + [0]*padding_after)
        label = labels[j]
        label_idx = label2id[label]
        sequence_batch.append(sequence_vec)
        label_batch.append(label_idx)
    return create_variable(torch.stack(sequence_batch)), create_variable(torch.LongTensor(label_batch))


# Some utility functions
def time_since(since):
    s = time.time() - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

class RNNClassifier(nn.Module):
    # Our model

    def __init__(self, input_size, hidden_size, output_size, n_layers=1, bidirectional=True):
        super(RNNClassifier, self).__init__()
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.n_directions = int(bidirectional) + 1

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, n_layers,
                          bidirectional=bidirectional)
        self.fc = nn.Linear(hidden_size, output_size)

    def forward(self, input):
        # Note: we run this all at once (over the whole input sequence)
        # input shape: B x S (input size)
        # transpose to make S(sequence) x B (batch)
        input = input.t()

        batch_size = input.size(1)

        # Make a hidden
        hidden = self._init_hidden(batch_size)

        # Embedding S x B -> S x B x I (embedding size)
        embedded = self.embedding(input)

        # To compact weights again call flatten_parameters().
        self.gru.flatten_parameters()
        output, hidden = self.gru(embedded, hidden)

        # Use the last layer output as FC's input
        # No need to unpack, since we are going to use hidden
        fc_output = self.fc(hidden[-1])
        return fc_output

    def _init_hidden(self, batch_size):
        hidden = torch.zeros(self.n_layers * self.n_directions,
                             batch_size, self.hidden_size)
        return create_variable(hidden)


if __name__ == '__main__':

    # input = sys.argv[1]
    # model = sys.argv[2]

    N_WORDS = 23624
    HIDDEN_SIZE = 100
    N_LABELS = 8
    N_LAYERS = 2
    classifier = RNNClassifier(N_WORDS, HIDDEN_SIZE, N_LABELS, N_LAYERS).cuda()
    classifier.load_state_dict(torch.load("data/rnn_model-1544344461.1830552"))
    classifier.eval()
    # classifier = nn.DataParallel(classifier)
    #
    # test_seq = torch.stack([torch.LongTensor([0, 0, sentence2vec([input])[0], 0, 0]).cuda()])
    # output = classifier(test_seq)
    # pred = output.data.max(1, keepdim=True)[1]
    # all_labels[pred.item()]
